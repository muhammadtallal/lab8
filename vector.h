#include <iostream>
#include <vector>
using namespace std;
class myVector{

	public:
	const int row;
	int * vec;
	myVector(int x) : row(x){
		vec = new int[x];
		for (int i = 0; i < x; i++){
			vec[i] = i;
		}
	}
};

vector<int> vecMul(myVector x, myVector y);