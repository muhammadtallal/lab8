#include "vector.h"
vector<int> vecMul(myVector x, myVector y){

	vector<int> Ans;
	int sum;
	for(int i=0;i<x.row;i++){
		sum = x.vec[i]*y.vec[i];
		Ans.push_back(sum);
	}
	
	return Ans;
}
