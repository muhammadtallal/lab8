
#include "matrix.h"
#include "vector.h"
using namespace std;

myMatrix M(5,5);
myMatrix Q(5,5);
myVector M1(5);
myVector Q1(5);
int main(){
	
		
		vector<int> Ans = vecMul(M1, Q1);
		
		vector<int> Ans1 = matMulByVal(M, Q);

		vector<int> Ans2 = matMulByRef(&M, &Q);

	//auto end = chrono::high_resolution_clock::now();
	cout << " Output of vector*vector: ";
	for (std::vector<int>::const_iterator i = Ans.begin(); i != Ans.end(); ++i)
		std::cout << *i << ' ';
	cout << "\n Output of matrix*matrix by value: ";
	for (std::vector<int>::const_iterator i = Ans.begin(); i != Ans.end(); ++i)
		std::cout << *i << ' ';
	cout << "\n Output of matrix*matrix by reference: ";
	for (std::vector<int>::const_iterator i = Ans.begin(); i != Ans.end(); ++i)
		std::cout << *i << ' ';
	

	getchar();
	return 0;
}


