#include "matrix.h"
vector<int> matMulByVal(myMatrix x, myMatrix y){
	vector<int> result;
	if (x.col == y.row){
		int sum;
		for (int i = 0; i < x.row; i++){
			for (int j = 0; j < y.col; j++){
				sum = 0;
				for (int k = 0; k < y.row; k++){
					sum += x.vec[i + k] * y.vec[j + k*y.col];
				}
				result.push_back(sum);
			}
		}
	}
	return result;
}

vector<int> matMulByRef(myMatrix *x, myMatrix *y){
	vector<int> result;
	if (x->col == y->row){
		int sum;
		for (int i = 0; i < x->row; i++){
			for (int j = 0; j < y->col; j++){
				sum = 0;
				for (int k = 0; k < y->row; k++){
					sum += x->vec[i + k] * (y->vec[j + k*(y->col)]);
				}
				result.push_back(sum);
			}
		}
	}
	return result;
}