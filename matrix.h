
#include <iostream>
#include <vector>
using namespace std;
class myMatrix{

public:
	const int size;
	const int row;
	const int col;
	int * vec;

	myMatrix(int x, int y) : row(x), col(y), size(x*y){
		vec = new int[size];
		for (int i = 0; i < size; i++){
			vec[i] = i;
		}
	}

};
vector<int> matMulByVal(myMatrix x, myMatrix y);
vector<int> matMulByRef(myMatrix *x, myMatrix *y);